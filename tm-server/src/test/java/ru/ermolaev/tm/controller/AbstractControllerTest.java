package ru.ermolaev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.ermolaev.tm.TaskManagerApplication;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.entity.User;
import ru.ermolaev.tm.repository.IProjectRepository;
import ru.ermolaev.tm.repository.ITaskRepository;
import ru.ermolaev.tm.repository.IUserRepository;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
public abstract class AbstractControllerTest {

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected IUserRepository userRepository;

    @Autowired
    protected ITaskRepository taskRepository;

    @Autowired
    protected IProjectRepository projectRepository;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @Autowired
    protected PasswordEncoder passwordEncoder;

    protected MockMvc mockMvc;

    protected static final String credentials = "admin";

    protected static final User userA = new User();

    protected static final Project projectA = new Project();

    protected static final Project projectB = new Project();

    protected static final Task taskA = new Task();

    protected static final Task taskB = new Task();

    @BeforeClass
    public static void prepareData() {
        @NotNull final String prA = "projectA";
        projectA.setName(prA);
        projectA.setDescription(prA);
        projectA.setUser(userA);

        @NotNull final String prB = "projectB";
        projectB.setName(prB);
        projectB.setDescription(prB);
        projectB.setUser(userA);

        @NotNull final String taskNameA = "taskA";
        taskA.setName(taskNameA);
        taskA.setDescription(taskNameA);
        taskA.setProject(projectA);
        taskA.setUser(userA);

        @NotNull final String taskNameB = "taskB";
        taskB.setName(taskNameB);
        taskB.setDescription(taskNameB);
        taskB.setProject(projectA);
        taskB.setUser(userA);
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();

        userA.setLogin(credentials);
        userA.setPasswordHash(passwordEncoder.encode(credentials));
        userRepository.save(userA);
        projectRepository.save(projectA);
        projectRepository.save(projectB);
        taskRepository.save(taskA);
        taskRepository.save(taskB);

        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(credentials, credentials);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void clear() {
        userRepository.deleteAll();
        projectRepository.deleteAll();
        taskRepository.deleteAll();
    }

}
