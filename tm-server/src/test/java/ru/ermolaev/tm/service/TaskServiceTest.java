package ru.ermolaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.ermolaev.tm.TaskManagerApplication;
import ru.ermolaev.tm.api.service.IProjectService;
import ru.ermolaev.tm.api.service.ITaskService;
import ru.ermolaev.tm.api.service.IUserService;
import ru.ermolaev.tm.dto.AuthUser;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.entity.Project;
import ru.ermolaev.tm.entity.Task;
import ru.ermolaev.tm.enumeration.RoleType;

import java.util.ArrayList;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = TaskManagerApplication.class)
public class TaskServiceTest {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private ITaskService taskService;

    @Autowired
    private IProjectService projectService;

    @Autowired
    private IUserService userService;

    private final static ProjectDTO preparedProjectA = new ProjectDTO();

    private final static ProjectDTO preparedProjectB = new ProjectDTO();

    private final static TaskDTO preparedTaskA = new TaskDTO();

    private final static TaskDTO preparedTaskB = new TaskDTO();

    private final static String admin = "admin";

    private final static String test = "test";

    private final AuthUser userA = new AuthUser(admin, admin, new ArrayList<>());

    private final AuthUser userB = new AuthUser(test, test, new ArrayList<>());

    private Project createdProjectA;

    private Project createdProjectB;

    private Task createdTaskA;

    private Task createdTaskB;

    @BeforeClass
    public static void prepare() {
        preparedProjectA.setName(admin);
        preparedProjectA.setDescription(admin);
        preparedProjectB.setName(test);
        preparedProjectB.setDescription(test);

        preparedTaskA.setName(admin);
        preparedTaskA.setDescription(admin);
        preparedTaskB.setName(test);
        preparedTaskB.setDescription(test);
    }

    @Before()
    public void prepareData() throws Exception {
        userA.setUserId(userService.create(admin, admin, RoleType.ADMINISTRATOR).getId());
        userB.setUserId(userService.create(test, test, RoleType.USER).getId());

        createdProjectA = projectService.createProject(userA.getUserId(), preparedProjectA);
        createdProjectB = projectService.createProject(userB.getUserId(), preparedProjectB);

        preparedTaskA.setProjectId(createdProjectA.getId());
        createdTaskA = taskService.createTask(userA.getUserId(), preparedTaskA);
        preparedTaskB.setProjectId(createdProjectB.getId());
        createdTaskB = taskService.createTask(userB.getUserId(), preparedTaskB);

        @NotNull final List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(admin, admin, authorities);
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void clearData() {
        userService.removeAll();
        projectService.removeAll();
        taskService.removeAll();
    }

    @Test
    public void updateByIdTest() throws Exception {
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        @NotNull final String name = "newName";
        @NotNull final String description = "newDescription";
        taskDTO.setId(createdTaskA.getId());
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        taskDTO.setProjectId(createdProjectA.getId());
        taskService.updateById(userA.getUserId(), taskDTO);
        @Nullable final Task task = taskService.findOneById(createdTaskA.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), createdTaskA.getId());
        Assert.assertEquals(task.getName(), name);
        Assert.assertEquals(task.getDescription(), description);
    }

    @Test
    public void countAllTasksTest() {
        Assert.assertEquals(2, taskService.countAllTasks().intValue());
    }

    @Test
    public void countByUserIdTest() throws Exception {
        Assert.assertEquals(1, taskService.countByUserId(userA.getUserId()).intValue());
        Assert.assertEquals(1, taskService.countByUserId(userB.getUserId()).intValue());
    }

    @Test
    public void countByProjectIdTest() throws Exception {
        Assert.assertEquals(1, taskService.countByProjectId(createdProjectA.getId()).intValue());
        Assert.assertEquals(1, taskService.countByProjectId(createdProjectB.getId()).intValue());
    }

    @Test
    public void countByUserIdAndProjectIdTest() throws Exception {
        Assert.assertEquals(1, taskService.countByUserIdAndProjectId(userA.getUserId(), createdProjectA.getId()).intValue());
        Assert.assertEquals(1, taskService.countByUserIdAndProjectId(userB.getUserId(), createdProjectB.getId()).intValue());
        Assert.assertEquals(0, taskService.countByUserIdAndProjectId(userA.getUserId(), createdProjectB.getId()).intValue());
        Assert.assertEquals(0, taskService.countByUserIdAndProjectId(userB.getUserId(), createdProjectA.getId()).intValue());
    }

    @Test
    public void findOneByIdTest() throws Exception {
        @Nullable final Task task = taskService.findOneById(createdTaskA.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), createdTaskA.getId());
    }

    @Test
    public void findOneByUserIdAndIdTest() throws Exception {
        @Nullable final Task task = taskService.findOneById(userB.getUserId(), createdTaskB.getId());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), createdTaskB.getId());
    }

    @Test
    public void findOneByNameTest() throws Exception {
        @Nullable final Task task = taskService.findOneByName(userA.getUserId(), createdTaskA.getName());
        Assert.assertNotNull(task);
        Assert.assertEquals(task.getId(), createdTaskA.getId());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, taskService.findAll().size());
    }

    @Test
    public void findAllByUserIdTest() throws Exception {
        Assert.assertEquals(1, taskService.findAllByUserId(userA.getUserId()).size());
        Assert.assertEquals(1, taskService.findAllByUserId(userB.getUserId()).size());
    }

    @Test
    public void findAllByProjectIdTest() throws Exception {
        Assert.assertEquals(1, taskService.findAllByProjectId(createdProjectA.getId()).size());
        Assert.assertEquals(1, taskService.findAllByProjectId(createdProjectB.getId()).size());
    }

    @Test
    public void findAllByUserIdAndProjectIdTest() throws Exception {
        Assert.assertEquals(1, taskService.findAllByUserIdAndProjectId(userA.getUserId(), createdProjectA.getId()).size());
        Assert.assertEquals(1, taskService.findAllByUserIdAndProjectId(userB.getUserId(), createdProjectB.getId()).size());
        Assert.assertEquals(0, taskService.findAllByUserIdAndProjectId(userA.getUserId(), createdProjectB.getId()).size());
        Assert.assertEquals(0, taskService.findAllByUserIdAndProjectId(userB.getUserId(), createdProjectA.getId()).size());
    }

    @Test
    public void removeOneByIdTest() throws Exception {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeOneById(createdTaskA.getId());
        Assert.assertEquals(1, taskService.findAll().size());
        @Nullable final Task task = taskService.findOneById(createdTaskA.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeOneByUserIdAndIdTest() throws Exception {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeOneById(userA.getUserId(), createdTaskA.getId());
        Assert.assertEquals(1, taskService.findAll().size());
        @Nullable final Task task = taskService.findOneById(createdTaskA.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeOneByNameTest() throws Exception {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeOneByName(userA.getUserId(), createdTaskA.getName());
        Assert.assertEquals(1, taskService.findAll().size());
        @Nullable final Task task = taskService.findOneById(createdTaskA.getId());
        Assert.assertNull(task);
    }

    @Test
    public void removeAllTest() {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeAll();
        Assert.assertEquals(0, taskService.findAll().size());
    }

    @Test
    public void removeAllByUserIdTest() throws Exception {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeAllByUserId(userA.getUserId());
        Assert.assertEquals(1, taskService.findAll().size());
    }

    @Test
    public void removeAllByProjectIdTest() throws Exception {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeAllByProjectId(createdProjectA.getId());
        Assert.assertEquals(1, taskService.findAll().size());
    }

    @Test
    public void removeAllByUserIdAndProjectIdTest() throws Exception {
        Assert.assertEquals(2, taskService.findAll().size());
        taskService.removeAllByUserIdAndProjectId(userA.getUserId(), createdProjectA.getId());
        Assert.assertEquals(1, taskService.findAll().size());
    }

}
