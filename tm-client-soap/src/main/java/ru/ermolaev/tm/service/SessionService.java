package ru.ermolaev.tm.service;

import org.springframework.stereotype.Component;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Component
public class SessionService {

    private List<String> cookieHeaders = new ArrayList<>();

    public List<String> getCookieHeaders() {
        return cookieHeaders;
    }

    public void setCookieHeaders(List<String> cookieHeaders) {
        this.cookieHeaders = cookieHeaders;
    }

    @SuppressWarnings("unchecked")
    public void saveCookieHeaders(Object port) {
        if (port == null) return;
        BindingProvider bindingProvider = (BindingProvider) port;
        final Map<String, Object> responseContext = bindingProvider.getResponseContext();
        final Map<String, Object> httpResponseHeaders = (Map<String, Object>) responseContext.get(MessageContext.HTTP_RESPONSE_HEADERS);
        cookieHeaders = (List<String>) httpResponseHeaders.get("Set-Cookie");
    }

    public void setListCookieRowRequest(Object port) {
        if (getRequestContext(port) != null && getHttpRequestHeaders(port) == null) {
            getRequestContext(port).put(MessageContext.HTTP_REQUEST_HEADERS, new LinkedHashMap<>());
        }
        final Map<String, Object> httpRequestHeaders = getHttpRequestHeaders(port);
        if (httpRequestHeaders == null) return;
        httpRequestHeaders.put("Cookie", cookieHeaders);
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> getHttpRequestHeaders(Object port) {
        final Map<String, Object> requestContext = getRequestContext(port);
        if (requestContext == null) return null;
        return (Map<String, Object>) requestContext.get(MessageContext.HTTP_REQUEST_HEADERS);
    }

    private Map<String, Object> getRequestContext(Object port) {
        if (port == null) return null;
        BindingProvider bindingProvider = (BindingProvider) port;
        return bindingProvider.getRequestContext();
    }

}
