package ru.ermolaev.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.endpoint.soap.AuthenticationSoapEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;
import ru.ermolaev.tm.service.SessionService;

import java.util.ArrayList;

@Component
public class LogoutListener extends AbstractListener {

    private final AuthenticationSoapEndpoint authenticationSoapEndpoint;

    @Autowired
    public LogoutListener(
            @NotNull final AuthenticationSoapEndpoint authenticationSoapEndpoint,
            @NotNull final SessionService sessionService
    ) {
        super(sessionService);
        this.authenticationSoapEndpoint = authenticationSoapEndpoint;
    }

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account.";
    }

    @Override
    @EventListener(condition = "@logoutListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[LOGOUT]");
        authenticationSoapEndpoint.logout();
        sessionService.setCookieHeaders(new ArrayList<>());
        System.out.println("[OK]");
    }

}
