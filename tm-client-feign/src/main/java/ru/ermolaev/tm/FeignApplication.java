package ru.ermolaev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import ru.ermolaev.tm.bootstrap.Bootstrap;

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class FeignApplication implements CommandLineRunner {

    @Autowired
    private ApplicationContext applicationContext;

    public static void main(@Nullable final String[] args) {
        SpringApplication.run(FeignApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        @NotNull final Bootstrap bootstrap = applicationContext.getBean(Bootstrap.class);
        bootstrap.run(args);
    }

}
