package ru.ermolaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    private String name = "";

    private String description = "";

    private String userId;

    private Date startDate;

    private Date completeDate;

    private Date creationDate;

    @Override
    public String toString() {
        return "Project [" +
                "Name='" + name + '\'' +
                ", Description='" + description + '\'' +
                ']';
    }

}
