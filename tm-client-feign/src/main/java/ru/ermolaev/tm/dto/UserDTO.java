package ru.ermolaev.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.ermolaev.tm.enumeration.RoleType;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractEntityDTO implements Serializable {

    private static final long serialVersionUID = 1001L;

    private String login = "";

    private String passwordHash = "";

    private String email = "";

    private String firstName = "";

    private String middleName = "";

    private String lastName = "";

    private List<RoleType> roles = Collections.singletonList(RoleType.USER);

    private Boolean locked = false;

    @Override
    public String toString() {
        return "LOGIN: " + login + '\n' +
                "E-MAIL: " + email + '\n' +
                "FIRST-NAME: " + firstName + '\n' +
                "MIDDLE-NAME: " + middleName + '\n' +
                "LAST-NAME: " + lastName + '\n' +
                "ROLES: " + roles
                ;
    }

}
