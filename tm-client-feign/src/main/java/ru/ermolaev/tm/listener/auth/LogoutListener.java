package ru.ermolaev.tm.listener.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.endpoint.IAuthenticationRestEndpoint;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.listener.AbstractListener;

import java.util.ArrayList;

@Component
public class LogoutListener extends AbstractListener {

    private final IAuthenticationRestEndpoint authenticationEndpoint;

    @Autowired
    public LogoutListener(
            @NotNull final IAuthenticationRestEndpoint authenticationEndpoint
    ) {
        this.authenticationEndpoint = authenticationEndpoint;
    }

    @NotNull
    @Override
    public String command() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account.";
    }

    @Override
    @EventListener(condition = "@logoutListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[LOGOUT]");
        authenticationEndpoint.logout();
        System.out.println("[OK]");
    }

}
