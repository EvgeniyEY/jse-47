package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.endpoint.ITaskRestEndpoint;
import ru.ermolaev.tm.listener.AbstractListener;

@Component
public abstract class AbstractTaskListener extends AbstractListener {

    protected ITaskRestEndpoint taskEndpoint;

    @Autowired
    public AbstractTaskListener(
            @NotNull final ITaskRestEndpoint taskEndpoint
    ) {
        this.taskEndpoint = taskEndpoint;
    }

}
