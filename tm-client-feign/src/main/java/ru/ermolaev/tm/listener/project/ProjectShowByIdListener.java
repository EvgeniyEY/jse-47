package ru.ermolaev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.endpoint.IProjectRestEndpoint;
import ru.ermolaev.tm.dto.ProjectDTO;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class ProjectShowByIdListener extends AbstractProjectListener {

    @Autowired
    public ProjectShowByIdListener(
            @NotNull final IProjectRestEndpoint projectEndpoint
    ) {
        super(projectEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return "project-show-by-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project by id.";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = projectEndpoint.findById(id);
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[COMPLETE]");
    }

}
