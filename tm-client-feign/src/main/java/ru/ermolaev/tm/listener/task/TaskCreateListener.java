package ru.ermolaev.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ermolaev.tm.api.endpoint.ITaskRestEndpoint;
import ru.ermolaev.tm.dto.TaskDTO;
import ru.ermolaev.tm.event.ConsoleEvent;
import ru.ermolaev.tm.util.TerminalUtil;

@Component
public class TaskCreateListener extends AbstractTaskListener {

    @Autowired
    public TaskCreateListener(
            @NotNull final ITaskRestEndpoint taskEndpoint
    ) {
        super(taskEndpoint);
    }

    @NotNull
    @Override
    public String command() {
        return "task-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Create a new task.";
    }

    @Override
    @EventListener(condition = "@taskCreateListener.command() == #event.name")
    public void handler(final ConsoleEvent event) throws Exception {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        @Nullable final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName(taskName);
        taskDTO.setProjectId(projectId);
        taskDTO.setDescription(description);
        taskEndpoint.createTask(taskDTO);
        System.out.println("[COMPLETE]");
    }

}
