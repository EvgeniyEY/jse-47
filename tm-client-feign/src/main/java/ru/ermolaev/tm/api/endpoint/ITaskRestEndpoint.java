package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ermolaev.tm.dto.TaskDTO;

import java.util.List;

@FeignClient(contextId = "ITaskRestEndpoint", name = "tm-server")
@RequestMapping(value = "/api/rest/task", produces = MediaType.APPLICATION_JSON_VALUE)
public interface ITaskRestEndpoint {

    @PostMapping(value = "/create")
    TaskDTO createTask(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) throws Exception;

    @PutMapping(value = "/updateById")
    TaskDTO updateById(
            @RequestBody @Nullable final TaskDTO taskDTO
    ) throws Exception;

    @NotNull
    @GetMapping(value = "/countAll")
    Long countAllTasks() throws Exception;

    @Nullable
    @GetMapping(value = "/findById/{id}")
    TaskDTO findById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception;

    @NotNull
    @GetMapping(value = "/findAll")
    List<TaskDTO> findAll() throws Exception;

    @DeleteMapping(value = "/removeById/{id}")
    void removeOneById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception;

}
