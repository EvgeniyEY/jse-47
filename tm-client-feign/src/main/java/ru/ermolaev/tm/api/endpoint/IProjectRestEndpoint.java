package ru.ermolaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.ermolaev.tm.dto.ProjectDTO;

import java.util.List;

@FeignClient(contextId = "IProjectRestEndpoint", name = "tm-server")
@RequestMapping(value = "/api/rest/project", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
public interface IProjectRestEndpoint {

    @PostMapping(value = "/create")
    ProjectDTO createProject(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) throws Exception;

    @PutMapping(value = "/updateById")
    ProjectDTO updateById(
            @RequestBody @Nullable final ProjectDTO projectDTO
    ) throws Exception;

    @NotNull
    @GetMapping(value = "/countAll")
    Long countAllProjects() throws Exception;

    @Nullable
    @GetMapping(value = "/findById/{id}")
    ProjectDTO findById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception;

    @NotNull
    @GetMapping(value = "/findAll")
    List<ProjectDTO> findAll() throws Exception;

    @DeleteMapping(value = "/removeById/{id}")
    void removeById(
            @PathVariable("id") @Nullable final String id
    ) throws Exception;

}
